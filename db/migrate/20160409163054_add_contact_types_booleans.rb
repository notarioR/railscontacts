class AddContactTypesBooleans < ActiveRecord::Migration
  def change
    add_column :contacts, :friends, :boolean, default: false
    add_column :contacts, :family, :boolean, default: false
    add_column :contacts, :coworker, :boolean, default: false
    remove_column :contacts, :contact_type
  end
end
