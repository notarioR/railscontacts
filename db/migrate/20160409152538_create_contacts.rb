class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :last_name
      t.string :email
      t.string :contact_type
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :contacts, :name
    add_index :contacts, :last_name
  end
end
