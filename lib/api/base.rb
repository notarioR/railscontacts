module API
  class Base < Grape::API

    before do
      error!("401 Unauthorized", 401) unless authenticated
    end

    helpers do
      def authenticated # => params: ?email=email@domain.com&password=pass
        user = User.find_by_email(params[:email])
        user && user.valid_password?(params[:password])
      end
    end

  mount API::V1::Users

  end
end