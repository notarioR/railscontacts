module API
  module V1
    module Entities
      class User < Grape::Entity
        expose :email,  documentation: { type: 'String', desc: 'Email'}
        expose :id,     documentation: { type: 'Integer', desc: 'id'}
      end
    end
  end
end