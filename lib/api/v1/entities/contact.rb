module API
  module V1
    module Entities
      class Contact < Grape::Entity
        expose :id,         documentation: { type: 'Integer', desc: 'Id'}
        expose :name,       documentation: { type: 'String', desc: 'Name'}
        expose :last_name,  documentation: { type: 'String', desc: 'Name'}
        expose :email,      documentation: { type: 'String', desc: 'Email'}
        expose :phone_numbers, using: API::V1::Entities::PhoneNumber
      end
    end
  end
end