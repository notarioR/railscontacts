module API
  module V1
    module Entities
      class PhoneNumber < Grape::Entity
        expose :id, documentation: { type: 'Integer', desc: 'Id'}
        expose :phone, documentation: { type: 'String', desc: 'Phone'}
      end
    end
  end
end