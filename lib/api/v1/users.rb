module API
  module V1
    class Users < Grape::API
      version 'v1' # path
      format :json

      before do
        @user = User.find_by(email: params[:email])
      end

      resource :users do #api/v1/users?email=username@doamin.com&password=pass
        desc "Return all users"
        get do
          present User.all, with: API::V1::Entities::User
        end
      end
      resource :user do #api/v1/user?email=username@doamin.com&password=pass
        desc "Return one user"
        get do
          present @user, with: API::V1::Entities::User
        end
      end
      resource :contacts do #api/v1/contacts?email=username@doamin.com&password=pass
        desc "Return contacts from one user"
        get do
          present @user.contacts, with: API::V1::Entities::Contact
        end
      end
    end
  end
end
