json.array!(@contacts) do |contact|
  json.extract! contact, :id, :name, :last_name, :email, :family, :friends, :coworker, :user_id
  json.url contact_url(contact, format: :json)
end
