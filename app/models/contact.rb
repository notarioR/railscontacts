class Contact < ActiveRecord::Base
  belongs_to :user
  has_many :phone_numbers, dependent: :destroy


  has_attached_file :avatar,
                    :styles => { :medium => "100x100>", :thumb => "50x50>" },
                    :default_url => "/assets/contacts/missing.gif",
                    :url  => "/assets/contacts/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/assets/contacts/:id/:style/:basename.:extension"

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  accepts_nested_attributes_for :phone_numbers, reject_if: lambda { |a| a[:phone].blank? }, allow_destroy: true

  validates :name, :last_name, :email, :user_id, presence: true
  validates :email, uniqueness: true

  def self.search(query)
    where("name like ? OR last_name like ?", "%#{query}%", "%#{query}%")
  end
end
